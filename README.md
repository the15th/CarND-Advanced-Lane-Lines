**Advanced Lane Finding Project**

The goals / steps of this project are the following:

* Compute the camera calibration matrix and distortion coefficients given a set of chessboard images.
* Apply a distortion correction to raw images.
* Use color transforms, gradients, etc., to create a thresholded binary image.
* Apply a perspective transform to rectify binary image ("birds-eye view").
* Detect lane pixels and fit to find the lane boundary.
* Determine the curvature of the lane and vehicle position with respect to center.
* Warp the detected lane boundaries back onto the original image.
* Output visual display of the lane boundaries and numerical estimation of lane curvature and vehicle position.

**The code for all the steps below is contained in the IPython notebook file** `./project/advanced-lane-finding.ipynb`


[//]: # (Image References)

[image1]: ./output_images/undistort_output_full_figure.png "Undistorted"
[image2]: ./output_images/undistort_test_full_figure.png "Road Transformed"
[image3]: ./output_images/thresh_grad_mag_dir.png "Tresh Grad Mag and Dir"
[image4]: ./output_images/thresh_gradx_r_schan.png "Tresh Grad X R and S-channel"
[image5]: ./output_images/undistorted_and_warped.png "Warped image"
[image6]: ./output_images/lines-found.png "Fit Visual"
[image7]: ./output_images/lines-found2.png "Fit Visual 2"
[image8]: ./output_images/lane-area-plotted-back.png "Output"
[video1]: ./output_videos/project_video.mp4 "Video"

### Camera Calibration

I started camera calibration  by preparing "object points", which will be the (x, y, z) coordinates of the chessboard corners in the world, assuming chessboard is fixed on the (x, y) plane at z=0, such that the object points are the same for each calibration image.  Thus, `objp` is just a replicated array of coordinates, and `objpoints` will be appended with a copy of it every time I successfully detect all chessboard corners in a test image.  `imgpoints` will be appended with the (x, y) pixel position of each of the corners in the image plane with each successful chessboard detection.  

I then used the output `objpoints` and `imgpoints` to compute the camera calibration and distortion coefficients using the `cv2.calibrateCamera()` function.  I applied this distortion correction to the test image using the `cv2.undistort()` function and obtained this result:
![alt text][image1]

### Pipeline (single images)

#### 1. Providing an example of a distortion-corrected image.

I then applied the distortion correction as above to one of the test road images, grabbed from a front-facing camera on a car, obtaining the following result in the image below:
![alt text][image2]

#### 2. Methods to create a thresholded binary image.

Applying a combination of gradient thresholds along x and y axis, magnitude and direction thresholds you lose the yellow line information in the binary image:
![alt text][image3]

To obtain a better results I used a combination of gradiend along x axis with R and S-channel color thresholds (thresholding steps at `code cell [4]` of the IPython notebook).  Here's an example of my output for this step:
![alt text][image4]

#### 3. Describe how (and identify where in your code) you performed a perspective transform and provide an example of a transformed image.

The code for my perspective transform includes a function called `perspective_transform()`, which appears inside `code cell [8]` of the IPython notebook.  The `perspective_transform()` function takes as inputs an image (`undistorted`) and defines inside source (`src`) and destination (`dst`) points.  I chose the hardcode the source and destination points in the following manner:

```python
src = np.float32(
    [[(img_size[0] / 2) - 35, img_size[1] / 2 + 100],
    [((img_size[0] / 6) + 25), img_size[1]],
    [(img_size[0] * 5 / 6 ) + 75, img_size[1]],
    [(img_size[0] / 2 + 70), img_size[1] / 2 + 100]])
dst = np.float32(
    [[(img_size[0] / 4), 0],
    [(img_size[0] / 4), img_size[1]],
    [(img_size[0] * 3 / 4), img_size[1]],
    [(img_size[0] * 3 / 4), 0]])
```

This resulted in the following source and destination points:

| Source        | Destination   |
|:-------------:|:-------------:|
| 585, 460      | 320, 0        |
| 203, 720      | 320, 720      |
| 1127, 720     | 960, 720      |
| 695, 460      | 960, 0        |

I verified that my perspective transform was working as expected by drawing the `src` and `dst` points onto a test image and its warped counterpart to verify that the lines appear parallel in the warped image:
![alt text][image5]

#### 4. Describe how (and identify where in your code) you identified lane-line pixels and fit their positions with a polynomial-

 As showed through code cells `[10] [11] and [12]` of Ipython notebook, I started using the thresholded binary warped image to took a histogram of the bottom half of the image and then found the peak of the left and right halves of the histogram. After I settled a number of sliding windows, placed around the line centers to find and follow the lines up to the top of the frame, stepping through the windows one by one inside a for-loop (`code cells [10]`). The next step was to extract left and right line pixel positions and fit on them a second order polynomial using numpy function `polyfit`:
```python
left_fit = np.polyfit(lefty, leftx, 2)
right_fit = np.polyfit(righty, rightx, 2)
```
![alt text][image6]

The green shaded area shows where I searched for the lines (`code cell [12]`).
![alt text][image7]

This is equivalent to using a customized region of interest for each frame of video

#### 5. Describe how (and identify where in your code) you calculated the radius of curvature of the lane and the position of the vehicle with respect to center.

I did calculus this in code cells `[13]` of Ipython notebook.

First I defined a y-value corresponding to the bottom of the image, where I want radius of curvature.
Then I calculated the radius of curvature based on pixel values, in pixel space, and then repeated after defining  conversions in x and y from pixels space to meters, assuming that if you're projecting a section of lane similar to the images above, the lane is about 30 meters long and 3.7 meters wide:


```python
ym_per_pix = 30/720 # meters per pixel in y dimension
xm_per_pix = 3.7/700 # meters per pixel in x dimension
# Fit new polynomials to x,y in world space
left_fit_cr = np.polyfit(ploty*ym_per_pix, leftx*xm_per_pix, 2)
right_fit_cr = np.polyfit(ploty*ym_per_pix, rightx*xm_per_pix, 2)
# Calculate the new radii of curvature
left_curverad = ((1 + (2*left_fit_cr[0]*y_eval*ym_per_pix + left_fit_cr[1])**2)**1.5) / np.absolute(2*left_fit_cr[0])
right_curverad = ((1 + (2*right_fit_cr[0]*y_eval*ym_per_pix + right_fit_cr[1])**2)**1.5) / np.absolute(2*right_fit_cr[0])
```

#### 6. Provide an example image of your result plotted back down onto the road such that the lane area is identified clearly.

I implemented this step in lines # through # code cells `[14]` of Ipython notebook in the function `map_lane()`.  I finally put it all together the previous steps, Here is an example of my result on a test image:

![alt text][image8]

---

### Pipeline (video)

#### 1. Provide a link to your final video output.  Your pipeline should perform reasonably well on the entire project video (wobbly lines are ok but no catastrophic failures that would cause the car to drive off the road!).

Here's a [link to my video result](./output_videos/project_video.mp4)

---

### Discussion

#### 1. Briefly discuss any problems / issues you faced in your implementation of this project.

## Where will your pipeline likely fail?  
The pipeline fails when you have on road mssing marking lines or not clearly visible(for example, not be able to distinguish the lines from the road surface). This means in lane lines location and polynomial fitting step we are going to have false positive "hot" pixel detection, the algorithm will detects other edge lines and consequently catastrophic failures that would cause the car to drive off the road.

## What could you do to make it more robust?
My project would be improved smoothing over the last n frames of video to obtain a cleaner result.

Insert sanity checks in the code to handle the  detected  lane lines that are problematic for some reason, check similar curvature, that they are separated by approximately the right distance horizontally and that they are roughly parallel.

Exploiting a Line() class for the left and right lane lines to keep track of recent detections and to perform sanity checks.
Finally, implementing a corner detector to find automatically all the needed source corners by perspective transformation.
